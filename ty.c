#include <locale.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>
#include <curses.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <libguile.h>

#define LINES_MIN 4
#define LINES_EXTRA 24
#define GAP_MIN_SIZE 24

#define GP_ADDR(b, idx)  (&((b)->beg[idx]))
#define GAP_BEG_ADDR(b)  (GP_ADDR(b, (b)->gap_beg))
#define GAP_END(b)       ((b)->gap_beg + (b)->gap_size)
#define GAP_END_ADDR(b)  (GP_ADDR(b, GAP_END(b)))

#define PRE_SIZE(b)  ((b)->gap_beg)
#define POST_SIZE(b) ((b)->end - GAP_END(b))

#define IN_GAP(b, p)   (p >= (b)->gap_beg && p < GAP_END(b))
#define CHAR_POS(b, p) (p + (p >= (b)->gap_beg ? (b)->gap_size : 0))
#define GET_CHAR(b, p) (*GP_ADDR(b, CHAR_POS(b, p)))
#define DATA_SIZE(b)   ((b)->end - (b)->gap_size)

#define FAIL(...) endwin();                     \
  printf(__VA_ARGS__);                          \
  exit(1)

typedef struct {
  char* beg;
  size_t gap_beg;
  size_t gap_size;
  size_t end;
} GapBuffer;

typedef struct {
  size_t col, row;
} Loc;

typedef struct {
  Loc loc;
  size_t max_col;
  size_t offset;
} Cursor;

typedef struct {
  GapBuffer* text;
  Cursor cursor;
  char* file_path;
  char* name;
} Buffer;

typedef struct {
  size_t width, height;
  Loc offset;
  Buffer* buffer;
} Window;

int last_key = 0;
int status = 0;
Window* active_window;

void gb_move_gap(GapBuffer*, size_t);

void* allocate(size_t size) {
  void* a = malloc(size);
  if (!a) {
    FAIL("Ran out of memory.\n");
  }
  return a;
}

#define ALLOC(ty) ((ty*)allocate(sizeof(ty)))

GapBuffer* gb_create(char* data, size_t len, size_t gap_beg, size_t gap_size) {
  assert(data || len == 0);
  assert(gap_beg <= len);
  GapBuffer* gb = ALLOC(GapBuffer);

  gb->end = len + gap_size;
  gb->beg = data;
  gb->gap_beg = gap_beg;
  gb->gap_size = gap_size;

  return gb;
}

Buffer* init_buffer() {
  Buffer* buf = ALLOC(Buffer);

  buf->text = gb_create((char*)allocate(GAP_MIN_SIZE), 0, 0, GAP_MIN_SIZE);
  Cursor c = {{0, 0}, 0, 0};
  buf->cursor = c;

  return buf;
}

char* file_name_from_path(char* path) {
  for(ssize_t i = strlen(path); i >= 0; i--) {
    if (path[i] == '/') return &path[i + 1];
  }
  return path;
}

Buffer* buffer_from_file(char* fname) {
  char* path = realpath(fname, NULL);
  FILE* f = fopen(path, "r");

  if (!f) {
    FAIL("Failed to open file.\n");
  }

  fseek(f, 0, SEEK_END);
  size_t size = ftell(f);
  rewind(f);

  char* data = (char*) allocate(size + GAP_MIN_SIZE);
  char* dst = data + GAP_MIN_SIZE;
  if (fread(dst, sizeof(char), size, f) != size) {
    FAIL("Failed to open file.\n");
  }

  Buffer* buf = ALLOC(Buffer);

  buf->text = gb_create(data, size, 0, GAP_MIN_SIZE);
  Cursor c = {{0, 0}, 0, 0};
  buf->cursor = c;
  buf->name = file_name_from_path(fname);
  buf->file_path = path;

  fclose(f);

  return buf;
}

void buffer_save_to_file(Buffer* buf) {
  if (!buf->file_path) {
    FAIL("No file to write to.\n");
  }

  FILE* f = fopen(buf->file_path, "w");
  if (!f) {
    FAIL("Failed to open file.");
  }

  fwrite(buf->text->beg, sizeof(char), buf->text->gap_beg, f);
  fwrite(GAP_END_ADDR(buf->text), sizeof(char), POST_SIZE(buf->text), f);

  fclose(f);
}

void gb_grow(GapBuffer* gb) {
  assert(gb->gap_size == 0);

  size_t post_size = POST_SIZE(gb);
  gb->gap_size += GAP_MIN_SIZE;
  gb->end += GAP_MIN_SIZE;
  gb->beg = realloc(gb->beg, gb->end);

  if (post_size) {
    memmove(GAP_END_ADDR(gb), GAP_BEG_ADDR(gb), post_size);
  }
}

void gb_insert(GapBuffer* gb, char ch) {
  assert(gb->gap_size > 0);
  gb->beg[gb->gap_beg++] = ch;
  gb->gap_size--;

  if (!gb->gap_size) {
    gb_grow(gb);
  }
}

void gb_delete(GapBuffer* gb) {
  if (gb->gap_beg == 0) return;
  gb->gap_beg--;
  gb->gap_size++;
}

void gb_move_gap(GapBuffer* gb, size_t index) {
  if (index == gb->gap_beg) return;
  assert(index >= 0 && index <= DATA_SIZE(gb));

  ssize_t diff;

  if (index > gb->gap_beg) {
    diff = index - gb->gap_beg;
    memmove(GAP_BEG_ADDR(gb), GAP_END_ADDR(gb), diff + GAP_END(gb) - gb->gap_beg);
    gb->gap_beg = index;
  } else {
    diff = gb->gap_beg - index;
    memmove(GP_ADDR(gb, index + gb->gap_size), GP_ADDR(gb, index), gb->gap_beg - index);
    gb->gap_beg = index;
  }
}

size_t find_beg_of_line(Buffer* buf, size_t offset) {
  if (offset == 0) return 0;
  if (offset == DATA_SIZE(buf->text) && GET_CHAR(buf->text, offset - 1) == '\n') return DATA_SIZE(buf->text);
  if (GET_CHAR(buf->text, offset) == '\n') offset--;

  for(;;) {
    char ch = GET_CHAR(buf->text, offset);
    if (ch == '\n') return offset + 1;
    if (offset == 0) break;
    offset--;
  }

  return 0;
}

size_t find_end_of_line(Buffer* buf, size_t offset) {
  if (offset == DATA_SIZE(buf->text) && offset != 0) return DATA_SIZE(buf->text);

  for(; offset < DATA_SIZE(buf->text); offset++) {
    char ch = GET_CHAR(buf->text, offset);
    if (ch == '\n') return offset;
  }

  return offset;
}

size_t line_length(Buffer* buf, size_t offset) {
  return find_end_of_line(buf, offset) - find_beg_of_line(buf, offset);
}

void move_char(Buffer* buf, bool forward) {
  if (!forward && buf->cursor.offset == 0) return;
  if (forward && buf->cursor.offset == DATA_SIZE(buf->text)) return;

  if (forward) {
    char ch = GET_CHAR(buf->text, buf->cursor.offset);
    buf->cursor.offset++;

    if (ch == '\n') {
      buf->cursor.loc.col = 0;
      buf->cursor.loc.row++;
    } else {
      buf->cursor.loc.col++;
    }
  } else {
    buf->cursor.offset--;
    char ch = GET_CHAR(buf->text, buf->cursor.offset);

    if (ch == '\n') {
      buf->cursor.loc.row--;
      buf->cursor.loc.col = line_length(buf, buf->cursor.offset);
    } else {
      buf->cursor.loc.col--;
    }
  }
  buf->cursor.max_col = buf->cursor.loc.col;
}

void move_line(Buffer* buf, bool forward) {
  size_t nl_length;
  size_t new_offset;
  if (forward) {
    size_t line_end = find_end_of_line(buf, buf->cursor.offset);
    if (line_end == DATA_SIZE(buf->text)) return;
    new_offset = line_end + 1;
    nl_length = line_length(buf, new_offset);
  } else {
    size_t line_beg = find_beg_of_line(buf, buf->cursor.offset);
    if (line_beg == 0) return;
    new_offset = line_beg - 1;
    nl_length = line_length(buf, new_offset);
    new_offset -= nl_length;
  }

  size_t dst_col = nl_length < buf->cursor.max_col ? nl_length : buf->cursor.max_col;
  buf->cursor.loc.row += forward ? 1 : -1;
  buf->cursor.loc.col = dst_col;
  buf->cursor.offset = new_offset + dst_col;
}

void insert_char(Buffer* buf, char c) {
  gb_move_gap(buf->text, buf->cursor.offset);
  gb_insert(buf->text, c);
  move_char(buf, true);
}

void delete_char(Buffer* buf) {
  size_t where = buf->cursor.offset;
  if (where == 0) return;

  move_char(buf, false);
  gb_move_gap(buf->text, where);
  gb_delete(buf->text);
}

Window* create_window(size_t w, size_t h, Buffer* buf) {
  Window* win = ALLOC(Window);

  win->width = w;
  win->height = h;
  win->buffer = buf;
  Loc offset = {0, 0};
  win->offset = offset;

  return win;
}

bool is_in_window(Window* win, Loc loc) {
  ssize_t xdiff, ydiff;
  xdiff = loc.col - win->offset.col;
  ydiff = loc.row - win->offset.row;
  // TODO: Do proper margins
  return xdiff >= 0 && xdiff < (ssize_t)(win->width - 1) &&
    ydiff >= 0 && ydiff < (ssize_t)win->height;
}

void window_offset_to_cursor(Window* win) {
  if (is_in_window(win, win->buffer->cursor.loc)) return;

  size_t win_end = win->height + win->offset.row;
  size_t win_beg = win->offset.row;

  if (win->buffer->cursor.loc.row < win_beg) {
    win->offset.row = win->buffer->cursor.loc.row;
  } else if (win->buffer->cursor.loc.row >= win_end) {
    win->offset.row = win->buffer->cursor.loc.row - win->height + 1;
  }

  if (win->buffer->cursor.loc.col < win->offset.col) {
    win->offset.col = win->buffer->cursor.loc.col;
  } else if (win->buffer->cursor.loc.col >= win->offset.col + win->width - 1) {
    win->offset.col = win->buffer->cursor.loc.col - win->width + 2;
  }
}

void display_all(WINDOW* win) {
  Buffer* buf = active_window->buffer;
  int width, height;
  Loc offset;
  // TODO: calculate effective width (line numbers, etc.)
  width = active_window->width - 1;
  height = active_window->height;
  offset = active_window->offset;
  move(0, 0);

  size_t head = 0;
  char ch = 0;
  GapBuffer* gb = buf->text;
  size_t data_size = DATA_SIZE(gb);

  // TODO: Cache this infomation
  for(size_t i = 0; i < offset.row; i++) {
    // Walk the head to the next lines for each row we're offset
    head = find_end_of_line(buf, head) + 1;
  }

  bool eol = false;
  for(int i = 0; i < height; i++) {
    ssize_t dsize = 0;
    if (head < data_size) {
      addch(' ');
      size_t start = head + offset.col;
      size_t end = find_end_of_line(buf, head);
      eol = end < data_size;
      // How many displayable characters?
      ssize_t size = end - start;
      dsize = size < width ? size : width;
      if (dsize > 0) {
        for(int i = 0; i < dsize; i++) {
          addch(GET_CHAR(buf->text, start + i));
        }
      }
      head = end + 1;
    } else  {
      if (!eol) addch('~');
      eol = false;
    }
    if (dsize < (ssize_t)width) addch('\n');
    clrtoeol();
  }
  printw("%s | %lld:%lld", buf->name, 1 + active_window->buffer->cursor.loc.row, 1 + active_window->buffer->cursor.loc.col);

  clrtoeol();

  if (is_in_window(active_window, active_window->buffer->cursor.loc)) {
    curs_set(1);
    move(buf->cursor.loc.row - active_window->offset.row, 1 + buf->cursor.loc.col - active_window->offset.col);
  } else {
    curs_set(0);
  }
  refresh();
}

bool editor_input() {
  Buffer* buf = active_window->buffer;
  int ch;
  bool dirty = false;
  while ((ch = getch()) != ERR) {
    switch((last_key = ch)) {
    case KEY_BREAK: break;
    case KEY_DOWN:
      move_line(buf, true);
      dirty = true;
      break;
    case KEY_UP:
      move_line(buf, false);
      dirty = true;
      break;
    case KEY_LEFT:
      move_char(buf, false);
      dirty = true;
      break;
    case KEY_RIGHT:
      move_char(buf, true);
      dirty = true;
      break;
    case KEY_HOME: break;
    case KEY_F0: break;
    case KEY_F(1): break;
    case KEY_F(2): break;
    case KEY_F(3): break;
    case KEY_F(4): break;
    case KEY_F(5): break;
    case KEY_F(6): break;
    case KEY_F(7): break;
    case KEY_F(8): break;
    case KEY_F(9): break;
    case KEY_F(10): break;
    case KEY_F(11): break;
    case KEY_F(12): break;
    case KEY_DL: break;
    case KEY_IL: break;
    case KEY_DC: break;
    case KEY_IC: break;
    case KEY_EIC: break;
    case KEY_CLEAR: break;
    case KEY_EOS: break;
    case KEY_EOL: break;
    case KEY_SF: break;
    case KEY_SR: break;
    case KEY_NPAGE: break;
    case KEY_PPAGE: break;
    case KEY_STAB: break;
    case KEY_CTAB: break;
    case KEY_CATAB: break;
    case KEY_ENTER: break;

    case KEY_SRESET: break;
    case KEY_RESET: break;
    case KEY_PRINT: break;
    case KEY_LL: break;
    case KEY_A1: break;
    case KEY_A3: break;
    case KEY_B2: break;
    case KEY_C1: break;
    case KEY_C3: break;
    case KEY_BTAB: break;
    case KEY_BEG: break;
    case KEY_CANCEL: break;
    case KEY_CLOSE: break;
    case KEY_COMMAND: break;
    case KEY_COPY: break;
    case KEY_CREATE: break;
    case KEY_END: break;
    case KEY_EXIT: break;
    case KEY_FIND: break;
    case KEY_HELP: break;
    case KEY_MARK: break;
    case KEY_MESSAGE: break;
    case KEY_MOUSE: break;
    case KEY_MOVE: break;
    case KEY_NEXT: break;
    case KEY_OPEN: break;
    case KEY_OPTIONS: break;
    case KEY_PREVIOUS: break;
    case KEY_REDO: break;
    case KEY_REFERENCE: break;
    case KEY_REFRESH: break;
    case KEY_REPLACE: break;
    case KEY_RESIZE: break;
    case KEY_RESTART: break;
    case KEY_RESUME: break;
    case KEY_SAVE: break;
    case KEY_SBEG: break;
    case KEY_SCANCEL: break;
    case KEY_SCOMMAND: break;
    case KEY_SCOPY: break;
    case KEY_SCREATE: break;
    case KEY_SDC: break;
    case KEY_SDL: break;
    case KEY_SELECT: break;
    case KEY_SEND: break;
    case KEY_SEOL: break;
    case KEY_SEXIT: break;
    case KEY_SFIND: break;
    case KEY_SHELP: break;
    case KEY_SHOME: break;
    case KEY_SIC: break;
    case KEY_SLEFT: break;
    case KEY_SMESSAGE: break;
    case KEY_SMOVE: break;
    case KEY_SNEXT: break;
    case KEY_SOPTIONS: break;
    case KEY_SPREVIOUS: break;
    case KEY_SPRINT: break;
    case KEY_SREDO: break;
    case KEY_SREPLACE: break;
    case KEY_SRIGHT: break;
    case KEY_SRSUME: break;
    case KEY_SSAVE: break;
    case KEY_SSUSPEND: break;
    case KEY_SUNDO: break;
    case KEY_SUSPEND: break;
    case KEY_UNDO: break;

    case KEY_BACKSPACE:
    case 127:
      delete_char(buf);
      dirty = true;
      break;

    case 's':
      buffer_save_to_file(active_window->buffer);
      break;

    default:
      if (ch == 'q') { status = 1; }
      insert_char(buf, ch);
      dirty = true;
      break;
    }
  }

  if (dirty) window_offset_to_cursor(active_window);
  return dirty;
}

void init_guile() {
  scm_init_guile();
#include "ty.x"
}

int main(int argc, const char** argv) {
  setlocale(LC_ALL, "");

  WINDOW* win = initscr();

  init_guile();

  clear();
  noecho();
  cbreak();
  keypad(stdscr, true);
  nodelay(stdscr, true);

  Buffer* self = buffer_from_file("ty.c");
  size_t w, h;
  getmaxyx(win, h, w);
  // Leave room for the status line
  active_window = create_window(w, h - 1, self);

  display_all(win);

  while (status == 0) {
    if (editor_input()) {
      display_all(win);
    }
  }

  endwin();

  return EXIT_SUCCESS;
}
